import argparse
import sys
from PIL import Image, ImageDraw, ImageFont
import csv
import os
import shutil


def main(args):
    parser = argparse.ArgumentParser(description="Do something.")
    parser.add_argument("-f", "--filename", type=str, required=True)
    parser.add_argument("-n", "--namelist", type=str, required=True)
    args = parser.parse_args(args)
    if os.path.exists('output'):
        shutil.rmtree('output')
    os.mkdir("output")
    with open(args.namelist, newline='', encoding='utf-8') as csvfile:
        namereader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in namereader:
            makeImage(args.filename, row[0], row[1])
            print(', '.join(row))


def makeImage(baseImage: str, name: str, group: str):
    base = Image.open(baseImage).convert("RGBA")
    # make a blank image for the text, initialized to transparent text color
    txt = Image.new("RGBA", base.size, (255, 255, 255, 0))
    # get a font
    fnt = ImageFont.truetype("fonts/GreatVibes-Regular.ttf", 150)
    # get a drawing context
    d = ImageDraw.Draw(txt)
    # draw text, half opacity
    # d.text((10,10), "Hello", font=fnt, fill=(0,0,0,128))

    # draw text, full opacity
    # 355f4b
    d.text((700, 800), name, font=fnt, fill=(53, 95, 75, 255), anchor="ms")

    out = Image.alpha_composite(base, txt)
    if not os.path.exists("output/" + group):
        os.mkdir("output/" + group)
    out.save("output/" + group + "/" + name+'.png')


if __name__ == "__main__":
    main(sys.argv[1:])
