#!/bin/bash
virtualenv .venv
source .venv/bin/activate
pip install -r ./requirements.txt
pyinstaller --onefile main.py
cp -R fonts dist/