SET PYTHONIOENCODING="UTF-8"
virtualenv .venv
CALL .venv\Scripts\activate
pip install -r requirements.txt
pyinstaller --onefile main.py
xcopy fonts dist\fonts\ /s /y
CALL .venv\Scripts\deactivate