Add a name on a Certificate image with Python

# Usage
    usage: main.exe [-h] -f FILENAME -n NAMELIST

    Do something.

     optional arguments:
       -h, --help            show this help message and exit
       -f FILENAME, --filename FILENAME
       -n NAMELIST, --namelist NAMELIST

**FILENAME** is Certificate template which to add name text.

**NAMELIST** is CSV (comma seperated) that includes names and group name.

# Notes
build.bat and build.sh builds with pythoninstaller for Linux and Windows